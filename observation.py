from skimage.transform import rescale
import numpy as np


class Observation:
    @staticmethod
    def map_glimpse(img, action, attention_size):
        (x, y, sx, sy) = tuple(action)
        (x, y) = Observation.coordinates_from_normalised(img, x, y)
        (dx, dy) = (attention_size / 2) * np.asarray([sx, sy])
        return rescale(Observation.extract_centered_subarray(img, x, y, dx, dy),
                       scale=(sx, sy))

    @staticmethod
    def extract_centered_subarray(array, x, y, dx, dy):
        return array[x-dx:x+dx, y-dy:y+dy]

    @staticmethod
    def coordinates_from_normalised(img, x, y):
        return np.asarray(img.shape) * np.asarray([x, y])

    @staticmethod
    def get_center_coords(img):
        return np.asarray(img.shape) / 2
