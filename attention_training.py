import theano.tensor as T
from gaussian import Gaussian
from keras.callbacks import ModelCheckpoint
from keras.callbacks import EarlyStopping
import os


class AttentionTraining:
    def __init__(self, args, attention_model):
        self.args = args
        self.attention_model = attention_model

    def train(self, x, y):
        self.train_attention_nn(x, y)
        self.train_context_nn(x, y)
        self.train_classification_nn(x, y)

    def train_attention_nn(self, x, y):
        """
        Using the EM-algorithm from 'Learning Stochastic Feedforward Neural Networks', by Y. Tang et al.
        """
        theta = self.attention_model.attention_network.trainable_weights

        p_y_out = self.attention_model.classification_network.output
        p_y_in = self.attention_model.classification_network.activations  # flat activations -- bernoulli vars 0,1

        p_a_out = self.attention_model.classification_network.activations
        p_a_in = self.attention_model.attention_network.input

        # Define loss functions
        log_likelihood_p_y = Gaussian(p_y_in, p_y_out).log_likelihood
        log_likelihood_p_a = Gaussian(p_a_in, p_a_out).log_likelihood

        # Calculate importance weights
        w = T.vector("importance_weights")

        approx_likelihood = T.sum(w*(log_likelihood_p_a + log_likelihood_p_y))

        dt = T.grad(approx_likelihood, theta)

        self.attention_model.attention_network.trainable_weights = theta + dt


    def train_context_nn(self, x, y):
        outdir = self.args["outdir"]
        outfile = self.args["context_weights_savefile"]
        num_epochs = self.args["num_epochs"]
        batch_size = self.args["batch_size"]

        cp = ModelCheckpoint(filepath=os.path.join(outdir, outfile), save_best_only=True)
        es = EarlyStopping(monitor='val_loss', patience=3)

        self.attention_model.context_network.fit(x, y, shuffle=True, callbacks=[cp, es],
                                                 validation_split=0.1, nb_epoch=num_epochs, batch_size=batch_size)

    def train_classification_nn(self, x, y):
        outdir = self.args["outdir"]
        outfile = self.args["classification_weights_savefile"]
        num_epochs = self.args["num_epochs"]
        batch_size = self.args["batch_size"]

        cp = ModelCheckpoint(filepath=os.path.join(outdir, outfile), save_best_only=True)
        es = EarlyStopping(monitor='val_loss', patience=3)

        self.attention_model.context_network.fit(x, y, shuffle=True, callbacks=[cp, es],
                                                 validation_split=0.1, nb_epoch=num_epochs, batch_size=batch_size)
