import theano


def get_activations(model, layer_idx, x):
    """
    From https://github.com/fchollet/keras/issues/41
    """
    get_activations_th = theano.function(
        [model.layers[0].input],
        model.layers[layer_idx].get_output(train=False), allow_input_downcast=True
    )
    activations = get_activations_th(x)
    return activations
