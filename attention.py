from keras.models import Sequential
from keras.layers.core import Dense
from keras.layers.core import Dropout
from keras.layers.core import Flatten
from keras.layers.core import Reshape
from keras.layers.convolutional import Convolution2D
from keras.layers.recurrent import LSTM
from keras.engine.topology import Merge
from observation import Observation
from util import get_activations
import numpy as np


# Following the methodology outlined in 'Learning Wake-Sleep Recurrent Attention Models', NIPS 2015
# by Ba, J. et al.
class Attention:
    def __init__(self, args):
        self.is_first = True
        self.attention_size = eval(args["attention_size"])
        self.last_output = None

        # Build the NN models.
        self.attention_nn, self.num_params = Attention.build_attention_nn(args, self.attention_size)
        self.classification_nn = Attention.build_classification_nn(args)
        self.context_nn = Attention.build_context_nn(args, self.attention_size)
        self.inference_nn = Attention.build_inference_nn(args, self.attention_size)
        self.num_params += self.classification_nn.count_params() + self.context_nn.count_params()

    @property
    def attention_network(self):
        return self.attention_nn

    @property
    def classification_network(self):
        return self.classification_nn

    @property
    def context_network(self):
        return self.context_nn

    @property
    def inference_network(self):
        return self.inference_nn

    @staticmethod
    def build_attention_nn(args, attention_size):
        """
        Emits the next glimpse location based on the current image patch and action history
        held by recurrent layers.
        """
        lstm_size = args["lstm_size"]
        action_width = args["action_width"]
        cnn_kernel_size = eval(args["cnn_kernel_size"])
        n_filters = args["n_filters"]
        feature_vector_size = args["feature_vector_size"]
        n_filters_stage_2 = args["n_filters_stage_2"]
        cnn_kernel_size_stage_2 = eval(args["cnn_kernel_size_stage_2"])

        num_params = 0

        # Glimpse network -- image patch
        glimpse_image = Sequential()
        glimpse_image.add(Convolution2D(n_filters, cnn_kernel_size[0], cnn_kernel_size[1],
                                        activation='relu', border_mode='same',
                                        input_shape=(1, ) + attention_size))
        glimpse_image.add(Convolution2D(n_filters, cnn_kernel_size[0], cnn_kernel_size[1],
                                        activation='relu'))
        glimpse_image.add(Convolution2D(n_filters_stage_2, cnn_kernel_size_stage_2[0], cnn_kernel_size_stage_2[1],
                                        activation='relu'))
        glimpse_image.add(Flatten())
        glimpse_image.add(Dense(feature_vector_size, activation='linear'))

        # Glimpse network -- action
        glimpse_action = Sequential()
        glimpse_action.add(Dense(action_width, input_dim=action_width, activation='linear'))
        glimpse_action.add(Dense(feature_vector_size, activation='linear'))

        # Merge both action and image feature information via multiplication g_n = G_I * G_a
        model = Sequential()
        model.add(Merge([glimpse_image, glimpse_action], mode='mul'))
        model.add(Reshape((1, model.layers[0].output_shape[1],)))  # Adding number of time steps = 1

        # Recurrent 2-layer
        model.add(LSTM(lstm_size, activation='relu'))

        model.add(Reshape((1, model.layers[2].output_shape[1],)))  # Adding number of time steps = 1

        model.add(LSTM(lstm_size, activation='relu'))

        # Emission network
        model.add(Dense(action_width, activation='sigmoid'))

        num_params += glimpse_image.count_params() + glimpse_action.count_params() + model.count_params()

        return model, num_params

    @staticmethod
    def build_classification_nn(args):
        """
        Predicts the observed class based on the last recurrent layers state .
        """
        classification_size = args["classification_size"]
        lstm_size = args["lstm_size"]
        p_dropout = args["p_dropout"]
        n_classes = args["n_classes"]
        nn_optimizer = args["nn_optimizer"]

        model = Sequential()

        model.add(Dense(classification_size, input_dim=lstm_size, activation='relu'))
        model.add(Dropout(p_dropout))
        model.add(Dense(n_classes, activation='softmax'))

        model.compile(optimizer=nn_optimizer,
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])

        return model

    @staticmethod
    def build_context_nn(args, attention_size):
        """
        Provides the initial state for the last recurrent layer.
        """
        lstm_size = args["lstm_size"]
        kernel_size = eval(args["cnn_kernel_size"])
        n_filters = args["n_filters"]
        n_filters_stage_2 = args["n_filters_stage_2"]
        cnn_kernel_size_stage_2 = eval(args["cnn_kernel_size_stage_2"])
        nn_optimizer = args["nn_optimizer"]

        model = Sequential()

        model.add(Convolution2D(n_filters, kernel_size[0], kernel_size[1],
                                activation='relu', border_mode='same',
                                input_shape=(1, ) + attention_size))
        model.add(Convolution2D(n_filters, kernel_size[0], kernel_size[1],
                                activation='relu'))
        model.add(Convolution2D(n_filters_stage_2 * 2, cnn_kernel_size_stage_2[0], cnn_kernel_size_stage_2[1],
                                activation='relu'))
        model.add(Flatten())
        model.add(Dense(lstm_size, activation='linear'))

        model.compile(optimizer=nn_optimizer, loss='mse')

        return model

    @staticmethod
    def build_inference_nn(args, attention_size):
        # TODO: Not using an inference network for now.
        return Sequential()

    def next_glimpse(self, img):
        glimpse = self.subsequent_glimpse(img) if self.is_first else self.first_glimpse(img)
        return glimpse, self.classify(img)

    def first_glimpse(self, img):
        initial_observation = self.map_observation(img)

        # The initial observation is fed directly to the second layer
        # to prevent learning direct mappings from input.
        self.last_output = self.attention_nn.layers[1](initial_observation)
        self.is_first = False

        return self.last_output

    def subsequent_glimpse(self, img):
        self.last_output = self.attention_nn.predict(self.map_observation(img))
        return self.last_output

    def reset(self):
        self.is_first = True

    def map_observation(self, img):
        if self.last_output is None:
            action = np.concatenate([Observation.get_center_coords(img),
                                     np.asarray(img.shape) / self.attention_size])
        else:
            action = np.asarray(self.last_output)

        return Observation.map_glimpse(img, action, self.attention_size)

    def get_first_recurrent_activations(self, x):
        return get_activations(self.attention_nn, 2, x)  # 2 == idx of first recurrent layer

    def get_last_recurrent_activations(self, x):
        return get_activations(self.attention_nn, 4, x)  # 4 == idx of second recurrent layer

    def classify(self, x):
        return self.classification_nn.predict(self.get_first_recurrent_activations(x))

    def init_context(self, x):
        return self.context_nn.predict(self.get_first_recurrent_activations(x))
