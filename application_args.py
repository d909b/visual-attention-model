import argparse


class ApplicationArguments():
    @staticmethod
    def parse_args(argv):
        parser = argparse.ArgumentParser()

        parser.add_argument('--file',
                            help='The file to be processed.', required=True)
        parser.add_argument('--p_dropout',
                            help='The dropout rate used in training of the inference network. [0,1]',
                            default=0.4)
        parser.add_argument('--num_dense',
                            help='The number of hidden layers in the inference network. [1,n)',
                            default=2)
        parser.add_argument('--dense_size',
                            help='The width of the hidden layers in the inference network. [1,n)',
                            default=512)
        parser.add_argument('--lstm_size',
                            help='The width of the LSTM layers in the attention network. [1,n)',
                            default=512)
        parser.add_argument('--classification_size',
                            help='The width of the classification layer in the attention network. [1,n)',
                            default=256)
        parser.add_argument('--attention_size',
                            help='The size of the attention area output by the attention network. (x_width, y_width)',
                            default="(11, 11)")
        parser.add_argument('--train',
                            help='Whether the purpose of invocation is to train or to evaluate the model.',
                            default=False)
        parser.add_argument('--cnn_kernel_size',
                            help='Size of the kernel employed by convolutional layers.',
                            default='(3, 3)')
        parser.add_argument('--n_filters',
                            help='Number of filters employed by convolutional layers.',
                            default=64)
        parser.add_argument('--cnn_kernel_size_stage_2',
                            help='Size of the kernel employed by stage 2 convolutional layers.',
                            default='(5, 5)')
        parser.add_argument('--n_filters_stage_2',
                            help='Number of filters employed by stage 2 convolutional layers.',
                            default=128)
        parser.add_argument('--feature_vector_size',
                            help='Size of the feature vector output by the glimpse network.',
                            default=512)
        parser.add_argument('--n_classes',
                            help='The number of classes for prediction at the end of a glimpse sequence.',
                            default=10)
        parser.add_argument('--nn_optimizer',
                            help='The optimizer used for the context and classification networks.',
                            default='adam')
        parser.add_argument('--num_epochs',
                            help='The number of epochs used for training.',
                            default=50)
        parser.add_argument('--batch_size',
                            help='The batch size used for training.',
                            default=32)
        parser.add_argument('--outdir',
                            help='The output directory for trained models.',
                            default='./weights/')

        return ApplicationArguments.add_app_configuration(vars(parser.parse_args(argv)))

    @staticmethod
    def add_app_configuration(args):
        args['action_width'] = 4  # Format: (x, y, sx, sy)
        args['context_weights_savefile'] = 'context_weights.hdf5'
        args['classification_weights_savefile'] = 'classification_weights.hdf5'
        args['attention_weights_savefile'] = 'attention_weights.hdf5'
        return args
