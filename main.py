import sys
from application_args import ApplicationArguments
from application_train import ApplicationTrain
from application_eval import ApplicationEval


class Application:
    def __init__(self, argv):
        args = ApplicationArguments.parse_args(argv)
        self.app = ApplicationTrain(args) if args['train'] else ApplicationEval(args)

    def run(self):
        self.app.run()


if __name__ == '__main__':
    app = Application(sys.argv[1:])
    app.run()
