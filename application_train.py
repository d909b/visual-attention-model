from attention import Attention
from attention_training import AttentionTraining

class ApplicationTrain:
    def __init__(self, args):
        self.args = args
        self.attention = Attention(args)
        self.trainer = AttentionTraining(args, self.attention)

    def run(self):
        # TODO: Load data set
        x = []
        y = []

        self.trainer.train(x, y)

