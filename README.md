# Visual Attention for Text Classification

Prototype of a visual attention model for text classification. Instead of taking the whole image as input a neural network decides which parts of the image to focus on.

## Architecture

Inspired by [Ba et al. "Learning Wake-Sleep Recurrent Attention Models", 2015](http://www.psi.toronto.edu/publications/2015/Learning%20Wake-Sleep%20Recurrent%20Attention%20Models.pdf)

## Dependencies 
* Python 2.7
* NumPy
* Keras, with
* Theano backend